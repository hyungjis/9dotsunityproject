﻿using UnityEngine;
using System.Collections;

public class DoubleJumpEnabler : MonoBehaviour
{
    private PlayerMove playerMove;     
    private bool CanDoubleJump = true;
    private bool GroundedBool;

    void Start()
    {
        playerMove = GetComponent<PlayerMove>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && !GroundedBool && CanDoubleJump)
        {
            playerMove.Jump(playerMove.jumpForce);
            CanDoubleJump = false;
        }
    }

    void FixedUpdate()
    {
        GroundedBool = playerMove.animator.GetBool("Grounded");
        if (!CanDoubleJump)
        {
            if (GroundedBool)
            {
                CanDoubleJump = true;
            }
        }
    }
}