﻿using UnityEngine;
using System.Collections;

public class ConstantRotate : MonoBehaviour
{

    public float RotationsPerMinY = 0;
    public float RotationsPerMinX = 0;
    public float RotationsPerMinZ = 0;

    void Update()
    {
        Vector3 ConstantRotation = new Vector3(0, 0, 0);

        if (RotationsPerMinX > 0)
        {
            ConstantRotation.x = 6.0f * RotationsPerMinX * Time.deltaTime;
        }

        if (RotationsPerMinZ > 0)
        {
            ConstantRotation.z = 6.0f * RotationsPerMinZ * Time.deltaTime;
        }

        if (RotationsPerMinY > 0)
        {
            ConstantRotation.y = 6.0f * RotationsPerMinY * Time.deltaTime;
        }
        transform.Rotate(ConstantRotation); //rotates 50 degrees per second around z axis
    }
}