﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovePlatformAnywhere : MonoBehaviour {

	public float moveDirectionX;
	public float moveDirectionY;
	public float moveDirectionZ;
	public float speed = 1;
	Rigidbody rigidbody;
	
	Vector3 FinalLocation;
	Vector3 StartingPosition;
	Vector3 OurDirection;
	float OurDistance;
	
	bool MovingForward = true;
	List<GameObject> array;
	
	// Use this for initialization
	void Start () 
	{
		FinalLocation = new Vector3(transform.position.x + moveDirectionX, transform.position.y + moveDirectionY, transform.position.z +moveDirectionZ);
		StartingPosition = transform.position;
		OurDirection = FinalLocation - StartingPosition;
		OurDistance = OurDirection.magnitude;
		OurDirection.Normalize();
		
		array = new List<GameObject>();
	}
	
	void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
		if(rigidbody == null)
			rigidbody = gameObject.AddComponent<Rigidbody>();
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
		
		BoxCollider TriggerCollider = gameObject.AddComponent<BoxCollider>();
		TriggerCollider.isTrigger = true;
	}
	
	void FixedUpdate()
	{
		if(MovingForward)
		{
			rigidbody.MovePosition(transform.position + (OurDirection*speed*Time.deltaTime));
			if((transform.position - StartingPosition).magnitude >= OurDistance)
			{
				MovingForward = false;
			}
		}
		else if(!MovingForward)
		{
			rigidbody.MovePosition(transform.position - OurDirection*speed*Time.deltaTime);
			if((transform.position - StartingPosition).magnitude <= .1f)
			{
				MovingForward = true;
			}
		}	
		foreach( GameObject thing in array)
		{
			if(MovingForward)
				thing.transform.position = new Vector3(thing.transform.position.x + (OurDirection.x*speed*Time.deltaTime),thing.transform.position.y,thing.transform.position.z + (OurDirection.z*speed*Time.deltaTime));
			else if(!MovingForward)
				thing.transform.position = new Vector3(thing.transform.position.x - (OurDirection.x*speed*Time.deltaTime),thing.transform.position.y,thing.transform.position.z - (OurDirection.z*speed*Time.deltaTime));
		
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
			array.Add (other.gameObject);
	}
	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
			array.Remove(other.gameObject);
	}
}
