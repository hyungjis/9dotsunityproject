﻿using UnityEngine;
using System.Collections;

public class WaterInfiniteJumping : MonoBehaviour {
	
	//We'll use this to communicate with the playerMove.
	private PlayerMove playerMove;
	
	//We'll pick the grounded bool from the Animator and store here to know if we're grounded.
	private bool GroundedBool;
	
	//We'll store here if we're swimming or not
	private bool Swimming = false;
	
	// Use this for initialization
	void Start () {
		//Pick the Player Move component
		playerMove = GetComponent<PlayerMove>();
	}
	
	// Update is called once per frame
	void Update() {
		//If we receive a jump button down, we're not grounded and we are swimming...
		if (Input.GetButtonDown ("Jump") && !GroundedBool && Swimming) {
			//Do a jump with the first jump force! :D Or a third of it, because the first one is already too much for swimming
			playerMove.Jump (playerMove.jumpForce/3);
		}
	}
	
	//This is an udpate that is called less frequently
	void FixedUpdate () {
		//Let's pick the Grounded Bool from the animator, since the player grounded bool is private and we can't get it directly..
		GroundedBool = playerMove.animator.GetBool("Grounded");
	}
	
	void OnTriggerEnter(Collider other) {
		//If the object has the tag Water that means we're in water now...
		if(other.gameObject.tag=="Water") {
			Swimming=true;
			
			//If we have an animation for swimming we should set a bool to true or something
			//playerMove.animator.SetBool ("Swimming", true);
		}
	}
	
	void OnTriggerExit(Collider other) {
		//If a object with the gametag water exited my Trigger...
		if(other.gameObject.tag=="Water") {
			Swimming=false;
			
			//Let's turn our swimming bool off on the animator
			//playerMove.animator.SetBool ("Swimming", false);
		}
	}
	
}