﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

	public GameObject teleportTo;
	// Use this for initialization
	void Start () {
		
	}
	void Awake(){
		BoxCollider myBoxCollider = this.gameObject.GetComponent<BoxCollider>();
		if(myBoxCollider != null)
			myBoxCollider.isTrigger = true;
			
		CapsuleCollider myCapsuleCollider = this.gameObject.GetComponent<CapsuleCollider>();
		if(myCapsuleCollider != null)
			myCapsuleCollider.isTrigger = true;	
			
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
		Debug.Log("WE SHOULD TELEPORT NOW!");
			other.transform.position = teleportTo.transform.position;
		}
	}
	
}
