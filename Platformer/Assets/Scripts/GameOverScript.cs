﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {
	public GameObject PlayerModel;
	public GameObject Camera;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void CallMe(){
		onGUI();
		MakeCharacterInvisible();
	}
	void onGUI()
	{
		Camera.GetComponent<Timer>().ShowGameOver = true;
	}
	void MakeCharacterInvisible()
	{
		//GameObject.Destroy (PlayerModel);
		Destroy (GetComponent<PlayerMove>());
		gameObject.SetActive(false);
	}
}
