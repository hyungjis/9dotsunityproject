﻿using UnityEngine;
using System.Collections;

public class EnemyProjectileShooter : MonoBehaviour
{

    private CharacterMotor characterMotor;

    public GameObject ProjectileObject;//Not Necessary
    public Transform SpawnPosition;
    public float WaitBeforeFiring = 0.2f;
    public float CooldownTime = 1f;

    private GameObject LastFiredProjectile;


    private bool CanShoot;
    void Start()
    {
        characterMotor = GetComponent<CharacterMotor>();
        CanShoot = true;
    }

    void FixedUpdate()
    {
        if (characterMotor.DistanceToTarget > 0.03f && CanShoot)
        {
            StartCoroutine(WaitAndFire(WaitBeforeFiring));
            CanShoot = false;
            StartCoroutine(CoolDown(CooldownTime));
        }
    }

    IEnumerator WaitAndFire(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Vector3 spawnPos = Vector3.zero;

        if (!SpawnPosition)
        {
            spawnPos = gameObject.transform.position + gameObject.transform.forward * 2 + Vector3.up * 0.45f;
        }
        else
        {
            spawnPos = SpawnPosition.position;
        }

        if (!ProjectileObject)
        {
            LastFiredProjectile = CreateProjectile();
            LastFiredProjectile.transform.rotation = gameObject.transform.rotation;
            LastFiredProjectile.transform.position = spawnPos;
        }
        else
        {
            LastFiredProjectile = GameObject.Instantiate(ProjectileObject, spawnPos, gameObject.transform.rotation) as GameObject;
        }
        LastFiredProjectile.GetComponent<ScriptProjectile>().Owner = this.gameObject;
        LastFiredProjectile.GetComponent<ScriptProjectile>().Initialize();
    }

    IEnumerator CoolDown(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        CanShoot = true;
    }

    GameObject CreateProjectile()
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.AddComponent<ScriptProjectile>();
        go.AddComponent<SphereCollider>();
        Rigidbody gobody = go.AddComponent<Rigidbody>();
        gobody.isKinematic = true;
        gobody.useGravity = false;
        go.GetComponent<Collider>().isTrigger = true;
        return go;
    }

}