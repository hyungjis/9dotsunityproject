﻿//THIS SCRIPT IS OLD//
//USE MovePlatformAnywhere INSTEAD!!!//

using UnityEngine;
using System.Collections;

public class MovePlatformLeft : MonoBehaviour {

	public float movespeed = 5f; //how quickly the platform moves
	public float TravelDistance = 20f;    //how far to move before reverse
	Vector3 Direction = Vector3.right;      //direction of travel
	Vector3 StartPos;
	
	public void Start()
	{
		StartPos = transform.position;
	}
	
	public void Update(){
		transform.position = new Vector3(transform.position.x, StartPos.y, transform.position.z);
		if (transform.position.x > StartPos.x + TravelDistance)
			Direction = -Direction;
		
		if (transform.position.x < StartPos.x)
			Direction = Vector3.right;
		
		transform.Translate(Direction * movespeed * Time.deltaTime);
	}
	
	void Awake()
	{
		if(!GetComponent<Rigidbody>())
			gameObject.AddComponent<Rigidbody>();
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
		
		BoxCollider TriggerCollider = gameObject.AddComponent<BoxCollider>();
		TriggerCollider.isTrigger = true;
	}
	
	void OnTriggerEnter(Collider Other)
	{
		if(Other.gameObject.tag == "Player")
		{
			Other.transform.parent = transform;
		}
	}
	void OnTriggerExit(Collider Other)
	{
		if(Other.gameObject.tag == "Player")
		{
			Other.transform.parent = null;
		}
	}
}
