using UnityEngine;

public class Timer : MonoBehaviour {
	public bool useTimer = true;
	public float timeRemaining = 300;
	public bool ShowGameOver = false;

	void Start() {

	}

	void Update() {
		timeRemaining -= Time.deltaTime;
	}

	void OnGUI(){
		GUIStyle style = new GUIStyle();
		style.fontSize = 32;

		if(ShowGameOver)
		{
			string myString = "Game Over";
			float textWidth = myString.Length * 60;
			GUIStyle gameOverStyle = new GUIStyle();
			gameOverStyle.fontSize = 100;
			GUI.Label (new Rect(Screen.width/2 - textWidth/2, Screen.height/2, textWidth, 600), myString, gameOverStyle);
			return;
		}

		if(timeRemaining > 0 && useTimer){
			GUI.Label(new Rect(150,0,600,400), "Time Remaining : " + timeRemaining,style);
		}
		else if( timeRemaining <= 0){
			ShowGameOver = true;
		}
		/*else if(timeRemaining <= 0 && useTimer){
			string myString = "You Have Survived!";
			float textWidth = myString.Length * 61;
			style.fontSize = 100;
			GUI.Label(new Rect(Screen.width/2 - textWidth/2, Screen.height/2, textWidth, 600), myString, style);
		}*/
	}
}
