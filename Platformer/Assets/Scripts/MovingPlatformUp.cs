﻿//THIS SCRIPT IS OLD//
//USE MovePlatformAnywhere INSTEAD!!!//

using UnityEngine;
using System.Collections;

public class MovingPlatformUp : MonoBehaviour {
	
	public float movespeed = 10f; //how quickly the platform moves
	public float TravelDistance = 20f;    //how far to move before reverse
	Vector3 Direction = new Vector3(0,1,0);      //direction of travel
	Vector3 StartPos;
	
	public void Start()
	{
		StartPos = transform.position;
	}
	
	public void FixedUpdate(){
		if (transform.position.y > StartPos.y + TravelDistance)
			Direction = -Direction;
		
		if (transform.position.y < StartPos.y)
			Direction = Vector3.up;
		
		transform.Translate(Direction * movespeed * Time.deltaTime);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//================DON'T WORRY ABOUT THIS!==================
	void Awake()
	{
		//add kinematic rigidbody
		if(!GetComponent<Rigidbody>())
			gameObject.AddComponent<Rigidbody>();
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
		
		BoxCollider TriggerCollider = gameObject.AddComponent<BoxCollider>();
		TriggerCollider.isTrigger = true;
	}
	
	void OnTriggerEnter(Collider Other)
	{
		if(Other.gameObject.tag == "Player")
		{
			Other.transform.parent = transform;
		}
	}
	void OnTriggerExit(Collider Other)
	{
		if(Other.gameObject.tag == "Player")
		{
			Other.transform.parent = null;
		}
	}
}