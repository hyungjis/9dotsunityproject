README!!

This is the the Unity3D project and all of its assets. 
To properly load the project, make sure to have Unity3D downloaded, and then navigate to Platformer/Assets/Scenes/ThemePark.unity.
This file should be openable through Unity so double clicking it should work. 

If there's any issues just shoot me an email~

-Hyungjin Shin
hshin2013@gmail.com